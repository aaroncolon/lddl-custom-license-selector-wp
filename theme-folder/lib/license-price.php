<?php
/**
 * Get License Price
 *
 * @param  {String} $customerType the type of customer
 * @param  {String} $filmType     the type of film
 * @param  {String} $distro       the type of distribution
 * @param  {String} $budget       the budget range
 * @param  {String} $campusSize   the size of the campus
 * @return {Int|String} price of the license
 */
function teo_get_license_price($customerType, $filmType = null, $distro, $budget, $campusSize = null) {
  $price = '';

  // @NOTE film-producer-filmmaker and college-student-film-student have unique flows
  if ($customerType == 'film-producer-filmmaker') {
    $price = teo_get_price_film_producer($distro, $budget, $filmType, $price);
  }

  else if ($customerType == 'college-student-film-student') {
    $price = teo_get_price_student($distro, $campusSize, $price);
  }

  else if ($customerType == 'social-media-content-creator') {
    $price = teo_get_price_social_media($distro, $budget, $price);
  }

  else if ($customerType == 'music-supervisor') {
    $price = teo_get_price_music_supervisor($distro, $budget, $price);
  }

  else if ($customerType == 'programming-producer') {
    $price = teo_get_price_programming_producer($distro, $budget, $price);
  }

  else if ($customerType == 'video-game-developer') {
    $price = teo_get_price_video_game($distro, $budget, $price);
  }

  return $price;
}

function teo_get_price_film_producer($distro, $budget, $filmType, $price) {
  // Short Film
  if ($filmType == 'short-film') {
    if ($distro == 'web-streaming' || $distro == 'external') {
      switch($budget) {
        case '0-5':
          $price = 69;
          break;
        case '5-25':
          $price = 99;
          break;
        case '25-50':
          $price = 149;
          break;
        case '50-up':
          $price = 'quote';
          break;
        default:
          $price = 'quote';
          break;
      }
    }
  }

  // Feature Film
  else if ($filmType == 'feature-film') {
    if ($distro == 'web-streaming' || $distro == 'external') {
      switch($budget) {
        case '0-50':
          $price = 199;
          break;
        case '50-250':
          $price = 349;
          break;
        case '250-500':
          $price = 499;
          break;
        case '500-up':
          $price = 'quote';
          break;
        default:
          $price = 'quote';
          break;
      }
    }
  }

  // Trailer
  else if ($filmType == 'trailer') {
    if ($distro == 'web-streaming' || $distro == 'external') {
      switch($budget) {
        case '0-50':
          $price = 399;
          break;
        case '50-250':
          $price = 549;
          break;
        case '250-500':
          $price = 699;
          break;
        case '500-up':
          $price = 'quote';
          break;
        default:
          $price = 'quote';
          break;
      }
    }
  }

  return $price;
}

function teo_get_price_student($distro, $campusSize, $price) {
  if ($distro == 'web-streaming' || $distro == 'internal' || $distro == 'podcast') {
    switch ($campusSize) {
      case '0-500':
        $price = 79;
        break;
      case '501-1000':
        $price = 89;
        break;
      case '1001-10000':
        $price = 119;
        break;
      case '10001-25000':
        $price = 179;
        break;
      case '25000-up':
        $price = 239;
        break;
      default:
        $price = 'quote';
        break;
    }
  }

  return $price;
}


function teo_get_price_social_media($distro, $budget, $price) {
  if ($distro == 'web-streaming') {
    switch($budget) {
      case '0-5':
        $price = 69;
        break;
      case '5-25':
        $price = 99;
        break;
      case '25-50':
        $price = 149;
        break;
      case '50-up':
        $price = 'quote';
        break;
      default:
        $price = 'quote';
        break;
    }
  }

  return $price;
}

function teo_get_price_music_supervisor($distro, $budget, $price) {
  if ($distro == 'web-streaming' || $distro == 'podcast') {
    switch($budget) {
      case '0-5':
        $price = 69;
        break;
      case '5-25':
        $price = 99;
        break;
      case '25-50':
        $price = 149;
        break;
      case '50-up':
        $price = 'quote';
        break;
      default:
        $price = 'quote';
        break;
    }
  }

  else if ($distro == 'tv') {
    switch($budget) {
      case '0-5':
        $price = 149;
        break;
      case '5-25':
        $price = 249;
        break;
      case '25-50':
        $price = 399;
        break;
      case '50-up':
        $price = 499;
        break;
      default:
        $price = 'quote';
        break;
    }
  }

  return $price;
}

function teo_get_price_programming_producer($distro, $budget, $price) {
  if ($distro == 'vod') {
    $price = 'quote';
  }

  else if ($distro == 'web-streaming' || $distro == 'internal' || $distro == 'podcast') {
    switch($budget) {
      case '0-5':
        $price = 69;
        break;
      case '5-25':
        $price = 99;
        break;
      case '25-50':
        $price = 149;
        break;
      case '50-up':
        $price = 'quote';
        break;
      default:
        $price = 'quote';
        break;
    }
  }

  else if ($distro == 'tv') {
    switch($budget) {
      case '0-5':
        $price = 149;
        break;
      case '5-25':
        $price = 249;
        break;
      case '25-50':
        $price = 399;
        break;
      case '50-up':
        $price = 499;
        break;
      default:
        $price = 'quote';
        break;
    }
  }

  return $price;
}

function teo_get_price_video_game($distro, $budget, $price) {
  if ($distro == 'in-game') {
    switch($budget) {
      case '0-5':
        $price = 69;
        break;
      case '5-25':
        $price = 99;
        break;
      case '25-50':
        $price = 149;
        break;
      case '50-up':
        $price = 'quote';
        break;
      default:
        $price = 'quote';
        break;
    }
  }

  else if ($distro == 'trailer') {
    switch($budget) {
      case '0-5':
        $price = 99;
        break;
      case '5-25':
        $price = 199;
        break;
      case '25-50':
        $price = 299;
        break;
      default:
        $price = 'quote';
        break;
    }
  }

  return $price;
}
