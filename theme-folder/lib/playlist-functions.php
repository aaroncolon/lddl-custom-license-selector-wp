<?php
/**
 * Playlist / Email Functions
 */

function teo_get_song_ids_from_query_vars() {
  if (empty($_GET['song_ids'])) {  
    return [];
  }

  $ids = filter_input(INPUT_GET, 'song_ids', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_THOUSAND);

  // Create array from ids
  $ids = explode(',', $ids);

  return $ids;
}

/**
 * Get Genres (@NOTE triage refactor of existing function)
 */
function teo_lddl_get_genres($post_id) {
  $genres = get_the_terms( $post_id, 'genre' );
  $genre_colors = array();
  $genre_names  = array();
  if ( $genres && ! is_wp_error( $genres ) ) {
    foreach($genres as $genre) {
      $genre_colors[] = $genre->term_id;
      if(!isset($genre_names[$genre->term_id]) ) {
        $genre_names[$genre->term_id] = $genre->name;
      }
    }
  }
  return array(
    'genre_names' => $genre_names, 
    'genre_colors' => $genre_colors
  );
}

/**
 * Get Artists (@NOTE triage refactor of existing function)
 */
function teo_lddl_get_artists($post_id) {
  $artists = get_the_terms( $post_id, 'artist' );
  $artist_names = array();
  if ( $artists && ! is_wp_error( $artists ) ) {
    foreach($artists as $artist) {
      if(!isset($artist_names[$artist->term_id]) ) {
        $artist_names[$artist->term_id] = $artist->name;
      }
    }
  }
  return $artist_names;
}

/**
 * Get Song Waveforms (@NOTE triage refactor of existing function)
 * @REFACTOR
 */
function teo_lddl_get_song_waveforms($post_id) {
  // Song Preview + Waveform images
  $sngId        = $post_id;
  $file         = get_post_meta($post_id, '_song_preview', true);
  $filename_arr = explode('/', $file);
  $filename     = end($filename_arr);
  $san_filename = str_replace(array(' ', '.'),'_',$filename);

  // @NOTE there is no $sngId??? @TODO fix???
  $checkFile       = get_template_directory_uri()."/audioplayer/waves/".$san_filename.$sngId.".png";
  $checkFile2      = get_template_directory_uri()."/audioplayer/waves/".$san_filename.$sngId."prog.png";
  $checkFile1_path = get_template_directory()."/audioplayer/waves/".$san_filename.$sngId.".png";
  $checkFile2_path = get_template_directory()."/audioplayer/waves/".$san_filename.$sngId."prog.png";

  // if _song_preview doesn't exist, look for _downloadable_files
  if ($file == '') {
    $files   = get_post_meta($post->ID, '_downloadable_files', true);
    $mp3file = '';

    // if there are downloadable files
    if (!empty($files)) {
      // get the first file and break...
      foreach ($files as $file) {
        $file         = $file['file'];
        $sngId        = $post->ID;
        $filename_arr = explode('/', $file);
        $filename     = end($filename_arr);
        $san_filename = str_replace(array(' ', '.'),'_',$filename);
        $checkFile    = get_template_directory_uri()."/audioplayer/waves/".$san_filename.$sngId.".png";
        $checkFile2   = get_template_directory_uri()."/audioplayer/waves/".$san_filename.$sngId."prog.png";
        $checkFile1_path = get_template_directory()."/audioplayer/waves/".$san_filename.$sngId.".png";
        $checkFile2_path = get_template_directory()."/audioplayer/waves/".$san_filename.$sngId."prog.png";
        break;
      }
    }
  }

  return array(
    'checkFile'       => $checkFile,
    'checkFile2'      => $checkFile2,
    'checkFile1_path' => $checkFile1_path,
    'checkFile2_path' => $checkFile2_path,
    'file'            => $file
  );
} // waveforms data

/**
 * Send Playlist Email
 */
function teo_playlist_send_email($songs) {
  if (isset($_REQUEST['send_email']))  {
    $name=$_REQUEST['your-name'];
    $myemail=$_REQUEST['your-email'];
    $emails = array($myemail);
    
    $to=explode(',',$_REQUEST['email']);
    $allEmails  = array_merge($emails,$to);

    $message=$_REQUEST['your-message'];

    require $_SERVER['DOCUMENT_ROOT'].'/PHPMailer-master/PHPMailerAutoload.php';
    $html = "";
    foreach($songs as $song) {
      $html .= "<tr><td style='padding: 5px;'>".$song['title']."</td><td style='padding: 5px;'>".$song['artist']."</td><td style='padding: 5px;'>".$song['genre']."</td><td style='padding: 5px;'>".$song['price']."</td></tr>";
    }
    $body   = '<div class="table-responsive">
    <table   border="1px" cellpadding="0" cellspacing="0" style="background-color:#333; color:#FFF; border-color:#333;"> 
    <tr>
    <th style="padding: 5px;">Title</th>
    <th style="padding: 5px;">Composer</th>
    <th style="padding: 5px;">Genre</th>
    <th style="padding: 5px;">Price</th>
    </tr>'.$html.'</table></div>';

    foreach($allEmails as $email){
      $mail = new PHPMailer;
      $mail->setFrom('admin@domain.com', 'admin');
      $mail->addAddress($email);    

      $mail->isHTML(true);                                
      $mail->Subject = 'Here is the subject';

      if($myemail == $email){
        $greetings = 'Hi '.$name.'<br/><br/>Here is your favourite list of song where you have to choose your song and play them:<br/><br/>';
      } else {
        $greetings = 'Hi '.$email.'<br/>here is your favourite list of song where you have to choose your song and play them:<br/><br/>';
      }

      $mail->Body = $greetings.$body.'<br/>'.$message;
      if(!$mail->send()) {

      } else {

      }

    }
    echo '<div class="success"><p style="color:#fff;">Mail has been sent successfully</p></div>';
  }
}
