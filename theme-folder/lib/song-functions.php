<?php
/**
 * Song Functions
 */

/**
 * Get Song Length (@NOTE triage refactor of existing function)
 *
 * @param  {Int}    $id the product id
 * @param  {String} $file the filename
 * @return {String} song duration length
 */
function teo_get_song_duration($file) {
  $mp3file;
  $duration;

  $mp3file   = new MP3File($file);
  @$duration = $mp3file->getDuration();

  return $duration;
}

/**
 * Get Song Soundwave Images (@NOTE triage refactor of existing function)
 *
 * @param  {Int}   $id the product id
 * @return {Array} Soundwave Image paths
 */
function teo_get_song_soundwave_images($id) {
  $file;
  $files;
  $filename;
  $filename_arr = array();
  $checkFile;
  $checkFile2;
  $checkFile1_path;
  $checkFile2_path;

  // get file path
  $file = get_post_meta($id, '_song_preview', true);

  if ($file == '') {
    $files = get_post_meta($id, '_downloadable_files', true);
    if (!empty($files)) {
      foreach ($files as $file) {
        $file = $file['file'];
        break;
      }
    }
  }

  $filename_arr    = explode('/', $file);
  $filename        = end($filename_arr);
  $san_filename    = str_replace(array(' ', '.'),'_', $filename);   
  $checkFile1_path = get_template_directory()."/audioplayer/waves/".$san_filename.$id.".png";
  $checkFile2_path = get_template_directory()."/audioplayer/waves/".$san_filename.$id."prog.png";
  
  if (file_exists($checkFile1_path) && file_exists($checkFile2_path)) {
    $checkFile     = get_template_directory_uri()."/audioplayer/waves/".$san_filename.$id.".png";
    $checkFile2    = get_template_directory_uri()."/audioplayer/waves/".$san_filename.$id."prog.png";
  } 
  else {
    $checkFile     = get_template_directory_uri()."/audioplayer/wavesbg.png";
    $checkFile2    = get_template_directory_uri()."/audioplayer/wavesprog.png";
  }

  return array(
    'file'            => $file,
    'checkFile'       => $checkFile,
    'checkFile2'      => $checkFile2,
    'checkFile1_path' => $checkFile1_path,
    'checkFile2_path' => $checkFile2_path
  );
}
