<?php

/**
 * Get License Price
 *
 * @param  {Number} $id the current Product ID
 * @param  {Object} $cart the current Cart object
 * @return {Array}  the custom data
 */
function teo_get_license_price_handler() {
  $result = array();

  // Verify nonce
  if ( ! wp_verify_nonce($_POST['nonce'], 'teo_get_license_price_nonce') ):
    $result['code'] = 500;
    $result['message'] = 'Unable to retrieve license price. Nonce incorrect.';
    echo json_encode($result);
    exit();
  endif;

  // Verify inputs
  if ( ! array_key_exists('budget', $_POST) || 
       ! array_key_exists('campus_size', $_POST) || 
       ! array_key_exists('customer_type', $_POST) || 
       ! array_key_exists('distribution', $_POST) || 
       ! array_key_exists('film_type', $_POST) ) {
    $result['code'] = 500;
    $result['message'] = 'Unable to retrieve license price. Form fields missing.';
    echo json_encode($result);
    exit();
  }

  // Sanitize inputs
  $budget        = sanitize_text_field($_POST['budget']);
  $campus_size   = sanitize_text_field($_POST['campus_size']);
  $customer_type = sanitize_text_field($_POST['customer_type']);
  $distribution  = sanitize_text_field($_POST['distribution']);
  $film_type     = sanitize_text_field($_POST['film_type']);

  // Error handling

  // Find license price
  $license_price = teo_get_license_price($customer_type, $film_type, $distribution, $budget, $campus_size);

  // Return license price
  $result['code']          = 200;
  $result['message']       = 'success';
  $result['currency']      = html_entity_decode(get_woocommerce_currency_symbol()); 
  $result['license_price'] = $license_price;

  $result['budget']        = $budget;
  $result['campus_size']   = $campus_size;
  $result['customer_type'] = $customer_type;
  $result['distribution']  = $distribution;
  $result['film_type']     = $film_type;
  
  echo json_encode($result);
  exit();
} // teo_get_license_price_handler()
add_action('wp_ajax_teo_get_license_price', 'teo_get_license_price_handler');
add_action('wp_ajax_nopriv_teo_get_license_price', 'teo_get_license_price_handler');


/**
 * Get Custom Cart Item Data
 *
 * Gets the specified cart item data
 *
 * @param  {Number} $id the current Product ID
 * @param  {Object} $cart the current Cart object
 * @return {Array}  the custom data
 */
function teo_get_custom_cart_item_data($id, $cart) {
  $custom_cart_item_data = array();

  // Get the cart contents
  $cart_contents = $cart->cart_contents; // array

  // Iterate through cart and look for any custom data to pass to generate_cart_id
  foreach ($cart_contents as $item) {
    // Find the ID and save the custom data
    if ($item['product_id'] == $id) {
      // Custom Price
      if (!empty($item['teo_price_custom'])) {
        $custom_cart_item_data['teo_price_custom']  = $item['teo_price_custom'];
      }
      // License data
      if (!empty($item['teo_budget'])) {
        $custom_cart_item_data['teo_budget']        = $item['teo_budget'];  
      }
      if (!empty($item['teo_campus_size'])) {
        $custom_cart_item_data['teo_campus_size']   = $item['teo_campus_size'];
      }
      if (!empty($item['teo_customer_type'])) {
        $custom_cart_item_data['teo_customer_type'] = $item['teo_customer_type'];
      }
      if (!empty($item['teo_distribution'])) {
        $custom_cart_item_data['teo_distribution']  = $item['teo_distribution'];  
      }
      if (!empty($item['teo_film_type'])) {
        $custom_cart_item_data['teo_film_type']     = $item['teo_film_type'];
      }
      break;
    }
  }

  return $custom_cart_item_data;
}

/** 
 * WooCommerce Add Cart Item Data
 *
 * Add custom cart item data
 *
 * @param $cart_item_data
 * @param $product_id
 * @param $variation_id
 */
function teo_add_cart_item_data($cart_item_data, $product_id, $variation_id, $quantity) {
  return $cart_item_data;
}
// add_filter('woocommerce_add_cart_item_data', 'teo_add_cart_item_data', 10, 3 );

function teo_get_cart_item_from_session( $cartItemData, $cartItemSessionData, $cartItemKey ) {
  if ( !empty($cartItemSessionData['teo_price_custom']) ) {
    $cartItemData['teo_price_custom']  = $cartItemSessionData['teo_price_custom'];
  }
  if ( !empty($cartItemSessionData['teo_budget']) ) {
    $cartItemData['teo_budget']        = $cartItemSessionData['teo_budget'];  
  }
  if ( !empty($cartItemSessionData['teo_campus_size']) ) {
    $cartItemData['teo_campus_size']   = $cartItemSessionData['teo_campus_size'];
  }
  if ( !empty($cartItemSessionData['teo_customer_type']) ) {
    $cartItemData['teo_customer_type'] = $cartItemSessionData['teo_customer_type'];
  }
  if ( !empty($cartItemSessionData['teo_distribution']) ) {
    $cartItemData['teo_distribution']  = $cartItemSessionData['teo_distribution'];  
  }
  if ( !empty($cartItemSessionData['teo_film_type']) ) {
    $cartItemData['teo_film_type']     = $cartItemSessionData['teo_film_type'];
  }

  return $cartItemData;
}
add_filter( 'woocommerce_get_cart_item_from_session', 'teo_get_cart_item_from_session', 10, 3 );

function teo_get_item_data( $data, $cartItem ) {
  if (!empty($cartItem['teo_budget'])) {
    $data[] = array(
      'display' => teo_format_budget($cartItem['teo_budget']),
      'key'     => 'teo_budget',
      'name'    => 'Budget',
      'value'   => $cartItem['teo_budget']
    );
  }
  if (!empty($cartItem['teo_campus_size'])) {
    $data[] = array(
      'display' => teo_format_campus_size($cartItem['teo_campus_size']),
      'key'     => 'teo_campus_size',
      'name'    => 'Campus Size',
      'value'   => $cartItem['teo_campus_size']
    );
  }
  if (!empty($cartItem['teo_customer_type'])) {
    $data[] = array(
      'display' => teo_format_custom_data($cartItem['teo_customer_type']),
      'key'     => 'teo_customer_type',
      'name'    => 'Customer Type',
      'value'   => $cartItem['teo_customer_type']
    );
  }
  if (!empty($cartItem['teo_distribution'])) {
    $data[] = array(
      'display' => teo_format_custom_data($cartItem['teo_distribution']),
      'key'     => 'teo_distribution',
      'name'    => 'Distribution',
      'value'   => $cartItem['teo_distribution']
    );
  }
  if (!empty($cartItem['teo_film_type'])) {
    $data[] = array(
      'display' => teo_format_custom_data($cartItem['teo_film_type']),
      'key'     => 'teo_film_type',
      'name'    => 'Film Type',
      'value'   => $cartItem['teo_film_type']
    );
  }

  return $data;
}
add_filter( 'woocommerce_get_item_data', 'teo_get_item_data', 10, 2 );

function teo_add_order_item_meta( $itemId, $values, $key ) {
  if (!empty( $values['teo_price_custom'] )) {
    wc_add_order_item_meta( $itemId, 'teo_price_custom', $values['teo_price_custom'] );
  }
  if (!empty($values['teo_budget'])) {
    wc_add_order_item_meta( $itemId, 'teo_budget', $values['teo_budget']);
  }
  if (!empty($values['teo_campus_size'])) {
    wc_add_order_item_meta( $itemId, 'teo_campus_size', $values['teo_campus_size']);
  }
  if (!empty($values['teo_customer_type'])) {
    wc_add_order_item_meta( $itemId, 'teo_customer_type', $values['teo_customer_type']);
  }
  if (!empty($values['teo_distribution'])) {
    wc_add_order_item_meta( $itemId, 'teo_distribution', $values['teo_distribution']);
  }
  if (!empty($values['teo_film_type'])) {
    wc_add_order_item_meta( $itemId, 'teo_film_type', $values['teo_film_type']);
  }
}
add_action( 'woocommerce_add_order_item_meta', 'teo_add_order_item_meta', 10, 3 );

/** 
 * WooCommerce Before Calculate Totals
 *
 * Add custom cart item data
 *
 * @param $cart_obj
 */
function teo_before_calculate_totals( $cart_obj ) {
  if ( is_admin() && ! defined( 'DOING_AJAX' ) ) {
    return;
  }

  // Iterate through each cart item
  foreach( $cart_obj->get_cart() as $key => $value ) {
    if ( isset( $value['teo_price_custom'] ) ) {
      $price = $value['teo_price_custom'];

      // WooCommerce version < 3
      $value['data']->price = $price;

      // WooCommerce version 3+
      // $value['data']->set_price( ( $price ) );
    }
  }
}
add_action( 'woocommerce_before_calculate_totals', 'teo_before_calculate_totals', 10, 1 );


// add to cart ajax actions
add_action( 'wp_ajax_teo_addtocart', 'teo_add_to_cart' );
add_action( 'wp_ajax_nopriv_teo_addtocart', 'teo_add_to_cart' );
function teo_add_to_cart() {
  $result = array();

  // Verify nonce
  if ( ! wp_verify_nonce($_POST['nonce'], 'teo_add_to_cart_nonce') ):
    $result['code'] = 500;
    $result['message'] = 'Unable to add to cart. Nonce incorrect.';
    echo json_encode($result);
    exit();
  endif;

  // Verify inputs
  if ( ! array_key_exists('budget', $_POST) || 
       ! array_key_exists('campus_size', $_POST) || 
       ! array_key_exists('customer_type', $_POST) || 
       ! array_key_exists('distribution', $_POST) || 
       ! array_key_exists('film_type', $_POST) || 
       ! array_key_exists('product_id', $_POST) ) {
    $result['code'] = 500;
    $result['message'] = 'Unable to add to cart. Form fields missing.';
    echo json_encode($result);
    exit();
  }

  // Sanitize inputs
  $budget        = sanitize_text_field($_POST['budget']);
  $campus_size   = sanitize_text_field($_POST['campus_size']);
  $customer_type = sanitize_text_field($_POST['customer_type']);
  $distribution  = sanitize_text_field($_POST['distribution']);
  $film_type     = sanitize_text_field($_POST['film_type']);
  $product_id    = sanitize_text_field($_POST['product_id']);
  
  // Add custom_item_data
  $price_custom = teo_get_license_price($customer_type, $film_type, $distribution, $budget, $campus_size);

  $cart_item_data = array();

  // Only add custom_item_data that !empty()
  if (!empty($price_custom)) {
    $cart_item_data['teo_price_custom']  = $price_custom;  
  }
  if (!empty($budget)) {
    $cart_item_data['teo_budget']        = $budget;
  }
  if (!empty($campus_size)) {
    $cart_item_data['teo_campus_size']   = $campus_size;
  }
  if (!empty($customer_type)) {
    $cart_item_data['teo_customer_type'] = $customer_type;
  }
  if (!empty($distribution)) {
    $cart_item_data['teo_distribution']  = $distribution;
  }
  if (!empty($film_type)) {
    $cart_item_data['teo_film_type']     = $film_type;
  }

  // Add product to cart
  if ( class_exists( 'WooCommerce' ) ) {
    global $cart;
    $cart = WC()->instance()->cart;
    $cart->add_to_cart($product_id, 1, $variation_id, $variation, $cart_item_data);
  }

  // Generate the result
  $product = wc_get_product($product_id);

  $result['code']           = 200;
  $result['message']        = 'Item added to cart.';
  $result['budget']         = $budget;
  $result['campus_size']    = $campus_size;
  $result['customer_type']  = $customer_type;
  $result['distribution']   = $distribution;
  $result['film_type']      = $film_type;
  $result['id']             = $product_id;
  $result['price_original'] = $product->get_price();
  $result['price_custom']   = $price_custom;
  $result['cart_item_data'] = $cart_item_data;
  
  echo json_encode($result);
  exit();
} // teo_add_to_cart
