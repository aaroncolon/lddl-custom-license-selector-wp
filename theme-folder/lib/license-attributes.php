<?php
/**
 * License Attributes
 */

function teo_get_customer_types() {
  $customer_type = get_terms( array(
    'taxonomy' => 'pa_customer-type',
    'hide_empty' => false
  ) );
  return $customer_type;
}

function teo_get_distribution_types() {
  $distribution_types = get_terms( array(
    'taxonomy'   => 'pa_distribution-type',
    'parent'     => 0,
    'hide_empty' => false
  ) );

  // get distros for each Customer Type
  $social_media         = teo_get_distro_social_media($distribution_types);
  $music_supervisor     = teo_get_distro_music_supervisor($distribution_types);
  $programming_producer = teo_get_distro_programming_producer($distribution_types);
  $student              = teo_get_distro_student($distribution_types);
  $video_game           = teo_get_distro_video_game($distribution_types);
  $film                 = teo_get_distro_film($distribution_types);
  
  $distribution_types = array(
    'social-media-content-creator' => $social_media,
    'music-supervisor'             => $music_supervisor,
    'programming-producer'         => $programming_producer,
    'college-student-film-student' => $student,
    'video-game-developer'         => $video_game,
    'film-producer-filmmaker'      => $film
  );

  return $distribution_types;
}

function teo_get_distro_social_media($distribution_types) {
  $distros = array();

  // filter distros hardcode or just get child items???
  foreach ($distribution_types as $v) {
    if ($v->slug == 'web-streaming') {
      $distros[] = array(
        'name' => $v->name,
        'slug' => $v->slug
      );
      break;
    }
  }
  return $distros;
}

function teo_get_distro_music_supervisor($distribution_types) {
  $distros = array();

  // filter distros hardcode or just get child items???
  foreach ($distribution_types as $v) {
    if ($v->slug == 'web-streaming' || $v->slug == 'tv' || $v->slug == 'podcast') {
      $distros[] = array(
        'name' => $v->name,
        'slug' => $v->slug
      );
    }
  }
  return $distros;
}

function teo_get_distro_programming_producer($distribution_types) {
  $distros = array();

  foreach ($distribution_types as $v) {
    if ($v->slug == 'web-streaming' || $v->slug == 'internal' || $v->slug == 'tv' || $v->slug == 'podcast' || $v->slug == 'video-on-demand') {
      $distros[] = array(
        'name' => $v->name,
        'slug' => $v->slug
      );
    }
  }
  return $distros;
}

function teo_get_distro_student($distribution_types) {
  $distros = array();

  foreach ($distribution_types as $v) {
    if ($v->slug == 'web-streaming' || $v->slug == 'internal' || $v->slug == 'podcast') {
      $distros[] = array(
        'name' => $v->name,
        'slug' => $v->slug
      );
    }
  }
  return $distros;
}

function teo_get_distro_video_game($distribution_types) {
  $distros = array();

  foreach ($distribution_types as $v) {
    if ($v->slug == 'in-game' || $v->slug == 'trailer') {
      $distros[] = array(
        'name' => $v->name,
        'slug' => $v->slug
      );
    }
  }
  return $distros;
}

function teo_get_distro_film($distribution_types) {
  $distros = array();

  foreach ($distribution_types as $v) {
    if ($v->slug == 'web-streaming' || $v->slug == 'external') {
      $distros[] = array(
        'name' => $v->name,
        'slug' => $v->slug
      );
    }
  }
  return $distros;
}

function teo_get_production_budgets() {
  $production_budgets = get_terms( array(
    'taxonomy'   => 'pa_production-budget',
    'parent'     => 0,
    'hide_empty' => false
  ) );
  return $production_budgets;
}

function teo_get_production_budget_low() {
  $id = null;
  $budget_low = null;
  $budgets = teo_get_production_budgets();

  // Find the ID of budget-low
  foreach ($budgets as $v) {
    if ($v->slug == 'budget-low') {
      $id = $v->term_id;
      break;
    }
  }

  // Get child terms
  if ($id) {
    $budget_low = get_terms( array(
      'taxonomy'   => 'pa_production-budget',
      'parent'     => $id,
      'hide_empty' => false
    ) );

    // format data
    $budget_low = teo_format_budget_data($budget_low);
  }
  return $budget_low;
}

function teo_get_production_budget_high() {
  $id = null;
  $budget_high = null;
  $budgets = teo_get_production_budgets();

  // Find the ID of budget-high
  foreach ($budgets as $v) {
    if ($v->slug == 'budget-high') {
      $id = $v->term_id;
      break;
    }
  }

  // Get child terms
  if ($id) {
    $budget_high = get_terms( array(
      'taxonomy'   => 'pa_production-budget',
      'parent'     => $id,
      'hide_empty' => false
    ) );

    // format data
    $budget_high = teo_format_budget_data($budget_high);
  }
  return $budget_high;
}

function teo_format_budget_data($data) {
  $budget_data = array();

  foreach ($data as $v) {
    $budget_data[] = array(
      'name' => $v->name,
      'slug' => $v->slug
    );
  }
  return $budget_data;
}

function teo_get_film_types() {
  $film_types = get_terms( array(
    'taxonomy' => 'pa_film-type',
    'hide_empty' => false
  ) );
  return $film_types;
}

function teo_get_campus_size() {
  $campus_size = get_terms( array(
    'taxonomy' => 'pa_campus-size',
    'hide_empty' => false
  ) );
  return $campus_size;
}

function teo_format_budget($budget) {
  $currency = get_woocommerce_currency_symbol();

  // explode the string on `-`
  $pieces = explode('-', $budget);

  // add `k` to the end of each array item
  for ($i = 0; $i < count($pieces); $i++) {
    if ($pieces[$i] == '0') {
      $pieces[$i] = $currency . $pieces[$i];
    }
    else {
      $pieces[$i] = $currency . $pieces[$i] . 'k';  
    }
  }

  // join the string with a - separating
  $budget_formatted = implode('-', $pieces);

  return $budget_formatted;
}

function teo_format_campus_size($campus_size) {
  $appendPlus = false;
  $pieces = explode('-', $campus_size);
  for ($i = 0; $i < count($pieces); $i++) {
    if ($pieces[$i] == 'up') {
      $appendPlus = true;
      continue;
    }
    $pieces[$i] = number_format((int)$pieces[$i]);  
  }
  return ($appendPlus) ? $pieces[0].'+' : implode('-', $pieces);
}

function teo_format_custom_data($data) {
  // replace hyphens with spaces
  $string = str_replace('-', ' ', $data);

  // title case
  $string = ucwords($string);

  return $string;
}
