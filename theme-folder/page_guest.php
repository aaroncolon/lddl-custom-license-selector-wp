<?php 
/* 
Template name: Guest Page Template
*/
?>

<?php get_header(); ?>

<div id="ajax-content">
  <div class="col-sm-11 col-md-11 col-sm-offset-0 col-md-offset-0 col-xs-11 col-xs-offset-0 mobile-tweak custom-temp portfolio_section">
    <div class="blog-posts project-posts">

      <!-- @aaroncolon -->
      <div class="container-fluid">
        <div class="row">
          <?php
          $args = array(
            'post_type'      => 'teo_projects',
            'posts_per_page' => -1,
            'order'          => 'ASC',
            'meta_query'     => array(
              array(
                'key'     => 'placement',
                'value'   => 'guest',
                'compare' => 'LIKE'
              )
            )
          );
          $projects = new WP_Query($args);

          if ($projects->have_posts()):
          ?>

            <?php
            while ( $projects->have_posts() ) : $projects->the_post();
            ?>
              <?php 
              // // Add New Row Every 5th Item
              // if ($projects->current_post == 0 || ($projects->current_post % 4) == 0) { 
              //   echo '<div class="row">';
              // } 
              ?>
                <div class="project-post project-post--<?php echo ($projects->current_post); ?> col-xs-12 col-sm-4 col-md-3 matchHeight--byRow">
                  <a href="javascript:;" class="open-popup-player" data-mfp-src="#popup-<?php echo get_the_ID() ?>">
                    <div class="project-post__thumbnail-wrap">
                      <?php the_post_thumbnail(); ?>
                    </div>
                  </a>
                  <div class="mfp-hide" id="popup-<?php echo get_the_ID() ?>">
                    <div class="popup-player">
                      <div class="inner clearfix">

                        <div class="popup-player__image-wrap">
                          <div class="popup-player__image">
                            <?php the_post_thumbnail(); ?>
                          </div>
                        </div>

                        <div class="popup-player__song-list-wrap">
                          <div class="popup-player__table-wrap">
                            <table class="table popup-player__table">
                              <thead class="popup-player__table-thead">
                                <tr>
                                  <th class="popup-player__product-name-th">
                                    <h3 class="popup-player__product-heading popup-player__product-heading--title">Title</h3>
                                  </th>
                                  <th class="popup-player__product-length-th">
                                    <h3 class="popup-player__product-heading popup-player__product-heading--length">Length</h3>
                                  </th>
                                </tr>
                              </thead>
                              <tbody class="popup-player__table-tbody">
                                <?php
                                // Get Product details from WooCommerce Product ID
                                $songs = (int) get_post_meta(get_the_ID(), 'songs', true);

                                if ($songs):
                                  for ($i = 0; $i < $songs; $i++):
                                    $product_id      = get_post_meta(get_the_ID(), 'songs_'.$i.'_teo_song_sub_repeater', true);
                                    $product         = wc_get_product($product_id);
                                    $product_title   = $product->get_title();
                                    $song_data       = teo_get_song_soundwave_images($product_id);
                                    $duration        = teo_get_song_duration($song_data['file']); 
                                    $artists         = teo_lddl_get_artists($product_id);
                                    $require_login   = get_post_meta(get_the_ID(), 'songs_'.$i.'_require_login', true);
                                    $class_variation = (!is_user_logged_in() && $require_login === '1') ? 'lock' : 'play';
                                ?>
                                    <tr class="popup-player__table-tr popup-player__table-tr--<?php echo $class_variation; ?>">
                                      <td class="popup-player__product-name">
                                        <a 
                                          class="popup-player__product-link popup-player__product-link--<?php echo $class_variation; ?>" 
                                          <?php echo (!is_user_logged_in() && $require_login === '1') ? 'title="Sign Up / Login Required"' : '' ?> 
                                          href="javascript:;" 
                                          data-id="<?php echo esc_attr($product_id) ?>" 
                                          data-src="<?php echo esc_attr($song_data['file']) ?>" 
                                          data-img="<?php echo get_the_post_thumbnail_url($product_id) ?>" 
                                          data-artist="<?php echo esc_attr(implode(', ', $artists)) ?>" 
                                          data-title="<?php echo esc_attr($product_title) ?>" 
                                          data-bg="<?php echo esc_attr($song_data['checkFile']) ?>" 
                                          data-prog="<?php echo esc_attr($song_data['checkFile2']) ?>">
                                          <?php echo esc_html(ucwords($product_title)); ?>
                                        </a>
                                      </td>
                                      <td class="popup-player__product-length">
                                        <?php echo MP3File::formatTime($duration); ?>
                                      </td>
                                    </tr>

                                  <?php endfor; ?>

                                <?php else: ?>

                                  <tr class="popup-player__table-tr">
                                    <td class="popup-player__product-name">
                                      No Songs Found.
                                    </td>
                                    <td class="popup-player__product-length">
                                      &nbsp;
                                    </td>
                                  </tr>

                                <?php endif; ?>

                              </tbody>
                            </table>
                          </div><!-- / .popup-player__table-wrap -->
                        </div><!-- / .popup-player__song-list-wrap -->

                      </div><!-- / .inner -->
                    </div><!-- / .popup-player -->
                  </div><!-- / .mfp-hide -->
                </div><!-- / .project-post -->
              <?php 
              // // Add Closing Row After Every 4rd Item or after the last item
              // if ( (($projects->current_post + 1) % 4 == 0) || ($projects->current_post + 1) == $projects->found_posts) { 
              //   echo '</div><!-- / .row -->';
              // } 
              ?>
            <?php
            endwhile;
            wp_reset_postdata();
          else:
            echo '<p>No Songs Found.</p>';
          endif;
          ?>

        </div><!-- / .row -->
      </div><!-- / .container-fluid -->

      
    </div>
  </div>
</div>
<?php get_footer(); ?>
