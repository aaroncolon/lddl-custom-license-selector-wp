jQuery(function(){

  (function() {
    "use strict";

    var popupPlayer = {
      _class: {
        projectPostsWrap: 'project-posts',
        popup:            'open-popup-player',
        lock:             'popup-player__product-link--lock',
        play:             'popup-player__product-link--play',
        playing:          'popup-player__product-link--playing',
        playingTr:        'popup-player__table-tr--playing'
      },

      init: function() {
        this.cacheDom();
        this.bindEvents();
        this.initMagnificPopup();
        this.initMatchHeight();
      },

      cacheDom: function() {
        // The currently playing song
        this.$currentSong = null;

        // Projects Wrap
        this.$projectsWrap = jQuery('.' + this._class.projectPostsWrap);

        // Popup Player (delegation)
        this.$popupPlayer = jQuery('.popup-player');

        // Player
        this.$playerWrap    = jQuery('.audiogallery');
        this.player         = this.$playerWrap.find('#player_id')[0];
        this.$playerAudio   = this.$playerWrap.find('audio');
        this.$playerArtist  = this.$playerWrap.find('.the-artist');
        this.$playerBgImg   = this.$playerWrap.find('.scrub-bg-img');
        this.$playerProgImg = this.$playerWrap.find('.scrub-prog-img');
        this.$playerThumb   = this.$playerWrap.find('.the-thumb');
        this.$playerTitle   = this.$playerWrap.find('.the-name');

        // Login Modal
        this.$loginModal    = jQuery('#login_modal');
      },

      bindEvents: function() {
        var _this = this;
        this.$popupPlayer.on('click', '.popup-player__product-link', {_this: _this}, this.handleSongLink);
      },

      initMatchHeight: function() {
        jQuery('.matchHeight--byRow').matchHeight({
          byRow: true
        });
      },

      initMagnificPopup: function() {
        this.$projectsWrap.magnificPopup({
          delegate: '.' + this._class.popup,
          // alignTop: true,
          type: 'inline',
          mainClass: 'mfp-player-popup mfp-fade',
          closeOnBgClick: true,
          removalDelay: 300,
          closeMarkup: '<button title="%title%" type="button" class="mfp-close">&times;</button>'
        });
      },

      handleSongLink: function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();

        var $this = jQuery(this);

        if ($this.hasClass(e.data._this._class.lock)) {
          // Trigger Login Modal
          e.data._this.handleSongLinkLock();
        }
        else if ($this.hasClass(e.data._this._class.play)) {
          // If the current song isn't already loaded or loading
          if ($this.data('id') !== MyVar.currentSongId) {
            // Reset and cache the $currentSong
            e.data._this.resetCurrentSong($this);
          }

          // Play the song
          e.data._this.handleSongLinkPlay($this, e);
        }
        else {
          // Pause the song
          e.data._this.handleSongLinkPause($this);
        }
      },

      handleSongLinkLock: function() {
        // Close Popup Player
        this.closePopup();

        // Show Login Modal
        this.$loginModal.modal('show');
      },

      handleSongLinkPlay: function($this, e) {
        if ($this.data('id') !== MyVar.currentSongId) {
          this.$playerArtist.text($this.data('artist'));

          // Empty <audio> element
          this.$playerAudio.empty();

          this.$playerAudio.attr('src', $this.data('src'));

          this.$playerBgImg.attr('src', $this.data('bg'));

          this.$playerProgImg.attr('src', $this.data('prog'));

          this.$playerThumb.css('background-image', 'url('+ $this.data('img') +')');
        
          this.$playerTitle.text($this.data('title'));

          // set global currentSongId
          MyVar.currentSongId = $this.data('id');
        }

        this.playSong($this);

        // @NOTE iOS / Safari requires play as result of user gesture
        // https://webkit.org/blog/7734/auto-play-policy-changes-for-macos/
        // this.$playerAudio.one('canplay.teo', function() {
        //   e.data._this.playSong($this);
        // });
      },

      handleSongLinkPause: function($this) {
        this.pauseSong($this);
      },

      playSong: function($songLink) {
        // Play the Song
        var promise = this.player.api_play();

        if (promise !== undefined) {
          promise.then(function() {
            console.log('autoplay started');
          }).catch(function(error) {
            console.log('autoplay prevented', error);
          });
        }

        // Remove `play` class
        $songLink.removeClass(this._class.play);

        // Add `playing` class
        $songLink.addClass(this._class.playing);
        $songLink.closest('tr').addClass(this._class.playingTr);
      },

      pauseSong: function($songLink) {
        // Pause the Song
        this.player.api_pause_media();

        // Remove `playing` class
        $songLink.removeClass(this._class.playing);
        $songLink.closest('tr').removeClass(this._class.playingTr);

        // Add `play` class
        $songLink.addClass(this._class.play);
      },

      resetCurrentSong: function($songLink) {
        // reset existing currentSong
        if (this.$currentSong) {
          // Toggle Classes
          this.$currentSong.removeClass(this._class.playing);
          this.$currentSong.closest('tr').removeClass(this._class.playingTr);
          this.$currentSong.addClass(this._class.play);

          // Remove from memory
          this.$currentSong = null;
        }
        // cache new currentSong
        this.$currentSong = $songLink;
      },

      closePopup: function() {
        jQuery.magnificPopup.close();
      }

    };

    popupPlayer.init();
  })()

}); // ready

