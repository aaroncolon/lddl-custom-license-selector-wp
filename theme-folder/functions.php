<?php

// ...

add_action( 'after_setup_theme', 'teo_setup' );
if ( ! function_exists( 'teo_setup' ) ){
  function teo_setup(){
    
    // ...

    require get_template_directory() . '/lib/license-price.php';
    require get_template_directory() . '/lib/license-attributes.php';
    require get_template_directory() . '/lib/playlist-functions.php';
    require get_template_directory() . '/lib/song-functions.php';
    require get_template_directory() . '/lib/woocommerce-custom-data.php';
  }
}

// ...

function teo_register_cpt() {
    $teo_projects_labels = array(
      'name'               => _x( 'Projects', 'post type general name', '_s-textdomain' ),
      'singular_name'      => _x( 'Project', 'post type singular name', '_s-textdomain' ),
      'menu_name'          => _x( 'Projects', 'admin menu', '_s-textdomain' ),
      'name_admin_bar'     => _x( 'Project', 'add new on admin bar', '_s-textdomain' ),
      'add_new'            => _x( 'Add New', 'Project', '_s-textdomain' ),
      'add_new_item'       => __( 'Add New Project', '_s-textdomain' ),
      'new_item'           => __( 'New Project', '_s-textdomain' ),
      'edit_item'          => __( 'Edit Project', '_s-textdomain' ),
      'view_item'          => __( 'View Project', '_s-textdomain' ),
      'all_items'          => __( 'All Projects', '_s-textdomain' ),
      'search_items'       => __( 'Search Projects', '_s-textdomain' ),
      'parent_item_colon'  => __( 'Parent Projects:', '_s-textdomain' ),
      'not_found'          => __( 'No Projects found.', '_s-textdomain' ),
      'not_found_in_trash' => __( 'No Projects found in Trash.', '_s-textdomain' )
    );

    register_post_type('teo_projects', array(
      'labels'              => $teo_projects_labels,
      'description'         => '',
      'has_archive'         => false,
      'exclude_from_search' => true,
      'publicly_queryable'  => false,
      'show_ui'             => true,
      'show_in_menu'        => true,
      'menu_position'       => 7,
      'capability_type'     => 'post',
      'map_meta_cap'        => true,
      'hierarchical'        => false,
      'rewrite'             => array('slug' => 'projects', 'with_front' => false),
      'query_var'           => true,
      'supports'            => array('title','editor','thumbnail')
    ));
}
add_action( 'init', 'teo_register_cpt' );

// Loading css/js files into the theme
add_action('wp_enqueue_scripts', 'teo_scripts');
if ( !function_exists('teo_scripts') ) {
  function teo_scripts() {

    // ...

    wp_enqueue_script( 'match-height', get_template_directory_uri() . '/js/vendor/match-height/jquery.matchHeight-min.js', array('jquery'), '1.1.0', true);
    wp_enqueue_script( 'magnific-popup', get_template_directory_uri() . '/js/vendor/magnific-popup/magnific-popup.min.js', array('jquery'), '1.1.0', true);
    wp_enqueue_script( 'license', get_template_directory_uri() . '/js/license-dialog.min.js', array('jquery'), '', true);
    wp_enqueue_script( 'popup-player', get_template_directory_uri() . '/js/popup-player.js', array('jquery'), '', true);

    // ...
}

/* Advanced Custom Fields */
/* Dynamically Populate Select Field with WooCommerce Products (songs) */
function teo_acf_load_song_field_choices($field) {
    // reset choices
    $field['choices'] = array();

    // get WooCommerce Products
    $args = array(
        'post_type'      => 'product',
        'posts_per_page' => -1,
        'orderby'        => 'post_title',
        'order'          => 'ASC'
    );
    $products = new WP_Query($args);

    if ($products->have_posts()) {
        while ($products->have_posts()) {
            $products->the_post();
            $field['choices'][get_the_ID()] = esc_html(get_the_title());
        }
        wp_reset_postdata();
    }

    return $field;
}
add_filter('acf/load_field/name=teo_song', 'teo_acf_load_song_field_choices');
add_filter('acf/load_field/name=teo_default_song', 'teo_acf_load_song_field_choices');
add_filter('acf/load_field/name=teo_song_sub_repeater', 'teo_acf_load_song_field_choices');

// @NOTE add to cart actions moved to ./lib/woocommerce-custom-data.php

// ...
