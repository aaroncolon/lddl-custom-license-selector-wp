<?php
/**
 * License Dialog Template
 *
 * Used for the license modal popup
 */
?>

<?php 

// Create nonce
$nonce_get_license_price = wp_create_nonce('teo_get_license_price_nonce');
$nonce_add_to_cart       = wp_create_nonce('teo_add_to_cart_nonce');

// Get All License Attributes
$customer_types     = teo_get_customer_types();
$distribution_types = teo_get_distribution_types();
$budget_low         = teo_get_production_budget_low();
$budget_high        = teo_get_production_budget_high();
$film_types         = teo_get_film_types();
$campus_size        = teo_get_campus_size();

?>

<div id="license-dialog-wrap" class="license-dialog-wrap mfp-hide">
  <script>
    var teoLicenseData = {
      budget: {
        low:  <?php echo json_encode($budget_low) ?>,
        high: <?php echo json_encode($budget_high) ?>
      },
      customerType: {
        "social-media-content-creator": {
          distribution: <?php echo json_encode($distribution_types['social-media-content-creator']) ?>
        },
        "music-supervisor": {
          distribution: <?php echo json_encode($distribution_types['music-supervisor']) ?>
        },
        "programming-producer": {
          distribution: <?php echo json_encode($distribution_types['programming-producer']) ?>
        },
        "college-student-film-student": {
          distribution: <?php echo json_encode($distribution_types['college-student-film-student']) ?>
        },
        "video-game-developer": {
          distribution: <?php echo json_encode($distribution_types['video-game-developer']) ?>
        },
        "film-producer-filmmaker": {
          distribution: <?php echo json_encode($distribution_types['film-producer-filmmaker']) ?>
        }
      }
    };
  </script>

  <button class="modal-close--quote" style="display: none;">&times;<span class="sr-only"> close license quote dialog</span></button>

  <div class="license-dialog clearfix">
    <div class="container-fluid">
      <div class="row">

        <div class="col-sm-6 col-xs-12">
          <div class="license-song-wrap">
            <a href="javascript:;" class="license-song-link">
              <div class="license-song-image-wrap">
                <img class="license-song-image" src="#" alt="">
              </div>
              <div class="license-song-title-wrap">
                <h2 class="title--license-song"></h2>
              </div>
            </a>
          </div>
        </div>


        <div class="col-sm-6 col-xs-12">

          <div class="license-form-wrap">

            <h1 class="title--license-form">License Details</h1>

            <form id="license-form" class="license-form" action="" method="">

              <!-- Customer Type -->
              <div class="form-group form-group--customer-type">
                <label for="customer-type" class="sr-only">Customer Type</label>
                <div class="select-wrap">
                  <select class="form-control" id="customer-type" name="customer-type" required>
                    <option value="">Customer Type</option>
                    <?php
                    if ($customer_types) {
                      foreach ($customer_types as $customer) {
                        echo '<option value="'. $customer->slug .'">'. $customer->name .'</option>';
                      }
                    }
                    ?>
                  </select>
                </div>
              </div>

              <!-- Film Type -->
              <div class="form-group form-group--film-type" style="display: none;">
                <label for="film-type" class="sr-only">Film Type</label>
                <div class="select-wrap disabled">
                  <select class="form-control" id="film-type" name="film-type" disabled required>
                    <option value="">Film Type</option>
                    <?php
                    if ($film_types) {
                      foreach ($film_types as $type) {
                        echo '<option value="'. $type->slug .'">'. $type->name .'</option>';
                      }
                    }
                    ?>
                  </select>
                </div>
              </div>

              <!-- Distribution Type -->
              <div class="form-group form-group--distribution-type">
                <label for="distribution-type" class="sr-only">Distribution</label>
                <div class="select-wrap disabled">
                  <select class="form-control" id="distribution-type" name="distribution-type" disabled required>
                    <option value="">Distribution</option>
                    <?php
                    if ($distribution_types) {
                      foreach ($distribution_types as $distro) {
                        echo '<option value="'. $distro->slug .'">'. $distro->name .'</option>';
                      }
                    }
                    ?>
                  </select>
                </div>
              </div>

              <!-- Production Budget -->
              <div class="form-group form-group--production-budget">
                <label for="production-budget" class="sr-only">Production Budget</label>
                <div class="select-wrap disabled">
                  <select class="form-control" id="production-budget" name="production-budget" disabled required>
                    <option value="">Production Budget</option>
                    <?php
                    if ($budget_low) {
                      foreach ($budget_low as $budget) {
                        echo '<option value="'. $budget->slug .'">'. $budget->name .'</option>';
                      }
                    }
                    ?>
                  </select>
                </div>
              </div>

              <!-- Campus Size -->
              <div class="form-group form-group--campus-size" style="display: none;">
                <label for="campus-size" class="sr-only">Campus Size</label>
                <div class="select-wrap disabled">
                  <select class="form-control" id="campus-size" name="campus-size" disabled required>
                    <option value="">Campus Size</option>
                    <?php
                    if ($campus_size) {
                      foreach ($campus_size as $size) {
                        echo '<option value="'. $size->slug .'">'. $size->name .'</option>';
                      }
                    }
                    ?>
                  </select>
                </div>
              </div>

              <div class="form-group form-group--price" style="display: none;">
                <p class="license-price"></p>
              </div>

              <div class="form group">
                <input id="add-to-cart" type="button" class="btn" value="Add To Cart" disabled>
                <input id="get-quote" type="button" class="btn" value="Get Quote" disabled style="display: none;">
              </div>

              <input id="nonce-get-price" type="hidden" value="<?php echo $nonce_get_license_price; ?>">
              <input id="nonce-add-to-cart" type="hidden" value="<?php echo $nonce_add_to_cart; ?>">
            </form>

          </div><!-- /. license-form-wrap -->
        </div><!-- / .col -->

      </div><!-- / .row -->
    </div><!-- / .container-fluid -->

  </div><!-- / .license-dialog -->


  <div class="license-quote-wrap" style="display: none;">
    <p class="lede--license-quote">The license type you’ve selected requires a custom quote.</p>
    
    <div class="license-quote-form-wrap">
      <?php 
      echo do_shortcode('[contact-form-7 id="2786" title="License Quote"]');
      ?>
    </div>
  </div><!-- / .license-quote -->

</div><!-- / .license-dialog-wrap -->
